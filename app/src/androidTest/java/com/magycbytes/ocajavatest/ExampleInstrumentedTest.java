package com.magycbytes.ocajavatest;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.text.Html;
import android.text.Spanned;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.magycbytes.ocajavatest", appContext.getPackageName());

        Spanned spanned = Html.fromHtml("Which is the result of the following?<br><br><span class=\\&quot;dCode dNoWrap\\&quot;>14:&amp;#160;List&amp;#60;String&amp;#62;&amp;#160;numbers&amp;#160;=&amp;#160;new&amp;#160;ArrayList&amp;#60;&amp;#62;();<\\/span><br><span class=\\&quot;dCode dNoWrap\\&quot;>15:&amp;#160;numbers.add(\\&quot;4\\&quot;);&amp;#160;numbers.add(\\&quot;7\\&quot;);<\\/span><br><span class=\\&quot;dCode dNoWrap\\&quot;>16:&amp;#160;numbers.set(1,&amp;#160;\\&quot;5\\&quot;);<\\/span><br><span class=\\&quot;dCode dNoWrap\\&quot;>17:&amp;#160;numbers.add(\\&quot;8\\&quot;);<\\/span><br><span class=\\&quot;dCode dNoWrap\\&quot;>18:&amp;#160;numbers.remove(0);<\\/span><br><span class=\\&quot;dCode dNoWrap\\&quot;>19:&amp;#160;for&amp;#160;(String&amp;#160;number&amp;#160;:&amp;#160;numbers)&amp;#160;System.out.print(number);<\\/span>");
        int z = 10; z++;
    }
}
