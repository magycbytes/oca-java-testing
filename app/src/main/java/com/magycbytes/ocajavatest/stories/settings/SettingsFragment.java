package com.magycbytes.ocajavatest.stories.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.magycbytes.ocajavatest.R;

/**
 * Created by Alex on 02/11/2016.
 */

public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.app_preferences);
    }
}
