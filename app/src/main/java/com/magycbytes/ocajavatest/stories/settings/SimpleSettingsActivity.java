package com.magycbytes.ocajavatest.stories.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SimpleSettingsActivity extends AppCompatActivity {


    public static void start(Context context) {
        context.startActivity(new Intent(context, SimpleSettingsActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
