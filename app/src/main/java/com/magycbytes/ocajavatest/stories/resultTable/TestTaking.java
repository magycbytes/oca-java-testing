package com.magycbytes.ocajavatest.stories.resultTable;

/**
 * Created by Alex on 02/11/2016.
 */

public class TestTaking {

    private int mPercents;
    private long mDate;

    public int getPercents() {
        return mPercents;
    }

    public void setPercents(int percents) {
        mPercents = percents;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        mDate = date;
    }
}
