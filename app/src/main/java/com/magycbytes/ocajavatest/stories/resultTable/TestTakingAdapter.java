package com.magycbytes.ocajavatest.stories.resultTable;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.ocajavatest.R;

import java.util.List;

/**
 * Created by Alex on 02/11/2016.
 */

public class TestTakingAdapter extends RecyclerView.Adapter<TestTakingViewHolder> {

    private List<TestTaking> mTestTakings;

    public TestTakingAdapter(List<TestTaking> testTakings) {
        mTestTakings = testTakings;
    }

    @Override
    public TestTakingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_test_taking, parent, false);
        return new TestTakingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TestTakingViewHolder holder, int position) {
        holder.show(mTestTakings.get(position));
    }

    @Override
    public int getItemCount() {
        return mTestTakings.size();
    }
}
