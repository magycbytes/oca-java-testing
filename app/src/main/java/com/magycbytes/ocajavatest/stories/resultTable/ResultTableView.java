package com.magycbytes.ocajavatest.stories.resultTable;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.magycbytes.ocajavatest.R;

/**
 * Created by Alex on 02/11/2016.
 */

public class ResultTableView {

    private RecyclerView mTableList;

    ResultTableView(View view) {
        mTableList = (RecyclerView) view.findViewById(R.id.content_list);
    }

    void setAdapter(RecyclerView.Adapter adapter) {
        mTableList.setLayoutManager(new LinearLayoutManager(mTableList.getContext()));
        mTableList.setAdapter(adapter);
    }
}
