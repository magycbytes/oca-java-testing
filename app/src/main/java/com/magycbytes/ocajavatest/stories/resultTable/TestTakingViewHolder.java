package com.magycbytes.ocajavatest.stories.resultTable;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.magycbytes.ocajavatest.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Alex on 02/11/2016.
 */

public class TestTakingViewHolder extends RecyclerView.ViewHolder {

    private TextView mPercents;
    private TextView mTakingDate;

    TestTakingViewHolder(View view) {
        super(view);

        mPercents = (TextView) view.findViewById(R.id.taking_percents);
        mTakingDate = (TextView) view.findViewById(R.id.taking_date);
    }

    void show(TestTaking testTaking) {
        mPercents.setText(String.valueOf(testTaking.getPercents()));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault());
        String format = simpleDateFormat.format(new Date(testTaking.getDate()));
        mTakingDate.setText(format);
    }
}
