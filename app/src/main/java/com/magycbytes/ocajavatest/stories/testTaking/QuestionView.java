package com.magycbytes.ocajavatest.stories.testTaking;

import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.magycbytes.ocajavatest.R;
import com.magycbytes.ocajavatest.choices.BaseChoice;
import com.magycbytes.ocajavatest.choices.ChoicesFactory;
import com.magycbytes.ocajavatest.json.Question;
import com.magycbytes.ocajavatest.stories.testTaking.helpers.HandlerTags;
import com.magycbytes.ocajavatest.stories.testTaking.helpers.HtmlEntities;

/**
 * Created by Alex on 31/10/2016.
 */

class QuestionView {

    private TextView mCode;
    private TextView mQuestion;
    private LinearLayout mResponses;
    private BaseChoice mChoice;

    QuestionView(View view) {
        mCode = (TextView) view.findViewById(R.id.question_code);
        mQuestion = (TextView) view.findViewById(R.id.question_text);
        mResponses = (LinearLayout) view.findViewById(R.id.responses_layout);
    }

    void reset() {
        mResponses.removeAllViews();
    }

    void showQuestion(Question question) {
        showResponses(question);
        showQuestionContent(question);
    }

    String getResponses() {
        if (mChoice == null) return "";
        return mChoice.getChoices();
    }

    private void showQuestionContent(Question question) {
        String data = question.getQuestionDataList().get(0).getData();

        int index = data.indexOf("<br><br>");
        if (index != -1) {
            String substring = data.substring(0, index);
            mQuestion.setText(Html.fromHtml(HtmlEntities.decode(substring), null, new HandlerTags()));

            String decode = HtmlEntities.decode(data);
            Spanned spanned = Html.fromHtml(decode, null, new HandlerTags());
            mCode.setText(spanned, TextView.BufferType.SPANNABLE);
        } else {
            String decode = HtmlEntities.decode(data);
            Spanned spanned = Html.fromHtml(decode, null, new HandlerTags());
            mCode.setText("");
            mQuestion.setText(spanned, TextView.BufferType.SPANNABLE);
        }
    }

    private void showResponses(Question question) {
        mChoice = ChoicesFactory.getChoice(question.getAnswerNumber(), mResponses);
        mChoice.addResponses(question.getChoiceDataList());
    }
}
