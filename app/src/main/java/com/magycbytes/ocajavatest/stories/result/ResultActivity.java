package com.magycbytes.ocajavatest.stories.result;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.magycbytes.ocajavatest.R;
import com.magycbytes.ocajavatest.stories.testTaking.UserResponse;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    public static final String RESPONSES = "Responses";

    public static void start(Context context, ArrayList<UserResponse> responses) {
        Intent intent = new Intent(context, ResultActivity.class);
        intent.putParcelableArrayListExtra(RESPONSES, responses);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        ArrayList<UserResponse> responses = getIntent().getParcelableArrayListExtra(RESPONSES);

        ResultView resultView = new ResultView(findViewById(R.id.activity_result));
        resultView.show(responses);

        ResponseResultAdapter adapter = new ResponseResultAdapter(responses);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.content_list);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView.setAdapter(adapter);
    }
}
