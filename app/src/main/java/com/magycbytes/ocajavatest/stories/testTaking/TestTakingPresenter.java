package com.magycbytes.ocajavatest.stories.testTaking;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.magycbytes.ocajavatest.json.Question;
import com.magycbytes.ocajavatest.stories.testTaking.loader.TestLoader;
import com.magycbytes.ocajavatest.stories.testTaking.loader.TestLoaderResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 01/11/2016.
 */

class TestTakingPresenter implements LoaderManager.LoaderCallbacks<TestLoaderResponse> {

    private static final String QUESTIONS_IDS = "QuestionsIds";
    private StatsView mStatsView;
    private QuestionView mQuestionView;
    private List<Question> mQuestionList;
    private Context mContext;
    private LoaderManager mLoaderManager;
    private ArrayList<Integer> mQuestionsIds = new ArrayList<>(60);
    private List<UserResponse> mUserResponses = new ArrayList<>();

    TestTakingPresenter(StatsView statsView, QuestionView questionView, Context context, LoaderManager loaderManager) {
        mStatsView = statsView;
        mQuestionView = questionView;
        mContext = context;
        mLoaderManager = loaderManager;
    }


    void showQuestions() {
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(QUESTIONS_IDS, mQuestionsIds);
        mLoaderManager.initLoader(1, bundle, this);
    }

    boolean showNextQuestion() {
        int currentQuestionNumber = mStatsView.getCurrentQuestionNumber();

        UserResponse userResponse = new UserResponse();
        userResponse.setResponse(mQuestionView.getResponses());
        userResponse.setQuestionId(mQuestionList.get(currentQuestionNumber - 1).getId());
        userResponse.setCorrectResponse(mQuestionList.get(currentQuestionNumber - 1).getAnswerNumber());
        userResponse.setQuestionNumber(String.valueOf(currentQuestionNumber));
        mUserResponses.add(userResponse);

        if (currentQuestionNumber >= mQuestionList.size()) return false;

        mStatsView.nextQuestion();
        showQuestion(currentQuestionNumber);

        return true;
    }

    ArrayList<UserResponse> getUserResponses() {
        return (ArrayList<UserResponse>) mUserResponses;
    }

    void saveState(Bundle bundle) {
        mStatsView.saveState(bundle);
        bundle.putIntegerArrayList(QUESTIONS_IDS, mQuestionsIds);
    }

    void restoreState(Bundle bundle) {
        mStatsView.restoreState(bundle);
        showQuestion(mStatsView.getCurrentQuestionNumber());
    }

    private void showQuestion(int position) {
        mQuestionView.reset();
        if (mQuestionList != null) {
            mQuestionView.showQuestion(mQuestionList.get(position));
        }
        mStatsView.showCurrentQuestionNumber();
    }

    @Override
    public Loader<TestLoaderResponse> onCreateLoader(int id, Bundle args) {
        return new TestLoader(args.getIntegerArrayList(QUESTIONS_IDS), mContext);
    }

    @Override
    public void onLoadFinished(Loader<TestLoaderResponse> loader, TestLoaderResponse data) {
        mQuestionList = data.getTestQuestions();
        mQuestionsIds = (ArrayList<Integer>) data.getTestIds();
        showQuestion(mStatsView.getCurrentQuestionNumber());
        mStatsView.setTotalQuestionsNumber(String.valueOf(mQuestionList.size()));
    }

    @Override
    public void onLoaderReset(Loader<TestLoaderResponse> loader) {

    }
}
