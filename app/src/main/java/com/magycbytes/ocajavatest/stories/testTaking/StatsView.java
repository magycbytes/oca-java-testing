package com.magycbytes.ocajavatest.stories.testTaking;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.magycbytes.ocajavatest.R;

/**
 * Created by Alex on 01/11/2016.
 */

class StatsView {

    private static final String QUESTION_NUMBER = "QuestionNumber";
    private static final String TOTAL_SPENT_TIME = "TotalSpentTime";
    private static final String CURRENT_QUESTION_TIME = "CurrentQuestionTime";

    private TextView mQuestionNumber;
    private TextView mTotalQuestionsNumber;
    private TimeTextView mTotalTime;
    private TimeTextView mQuestionTime;
    private int mCurrentQuestionNumber = 0;

    StatsView(View rootView) {
        mQuestionNumber = (TextView) rootView.findViewById(R.id.question_nr);
        mTotalQuestionsNumber = (TextView) rootView.findViewById(R.id.total_questions_count);
        mTotalTime = new TimeTextView((TextView) rootView.findViewById(R.id.total_time));
        mQuestionTime = new TimeTextView((TextView) rootView.findViewById(R.id.question_time));
    }

    void startQuiz(String totalQuestionsCount) {
        startTimers();
        updateCurrentQuestionNumber();
        setTotalQuestionsNumber(totalQuestionsCount);
    }

    void setTotalQuestionsNumber(String questionsNumber) {
        mTotalQuestionsNumber.setText(questionsNumber);
    }

    void nextQuestion() {
        mQuestionTime.reset();
        updateCurrentQuestionNumber();
    }

    void startTimers() {
        stopTimers();
        mTotalTime.run();
        mQuestionTime.run();
    }

    void stopTimers() {
        mTotalTime.stop();
        mQuestionTime.stop();
    }

    void saveState(Bundle outState) {
        outState.putInt(QUESTION_NUMBER, mCurrentQuestionNumber);
        outState.putLong(TOTAL_SPENT_TIME, mTotalTime.getElapsedTime());
        outState.putLong(CURRENT_QUESTION_TIME, mQuestionTime.getElapsedTime());
    }

    void restoreState(Bundle savedState) {
        mCurrentQuestionNumber = savedState.getInt(QUESTION_NUMBER);
        showCurrentQuestionNumber();

        mTotalTime.setElapsedTime(savedState.getLong(TOTAL_SPENT_TIME));
    }

    int getCurrentQuestionNumber() {
        return mCurrentQuestionNumber;
    }

    private void updateCurrentQuestionNumber() {
        mCurrentQuestionNumber++;
        showCurrentQuestionNumber();
    }

    void showCurrentQuestionNumber() {
        if (mCurrentQuestionNumber < 1) mCurrentQuestionNumber = 1;
        mQuestionNumber.setText(String.valueOf(mCurrentQuestionNumber));
    }
}
