package com.magycbytes.ocajavatest.stories.testTaking;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.magycbytes.ocajavatest.R;
import com.magycbytes.ocajavatest.database.TestTakingTable;
import com.magycbytes.ocajavatest.stories.result.ResultActivity;
import com.magycbytes.ocajavatest.util.ResultCalculator;

public class TestTakingActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG = TestTakingActivity.class.getSimpleName();

    private StatsView mStatsView;
    private TestTakingPresenter mMainPresenter;

    public static void start(Context context) {
        context.startActivity(new Intent(context, TestTakingActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_taking);

        QuestionView questionView = new QuestionView(findViewById(R.id.root_layout));
        mStatsView = new StatsView(findViewById(R.id.root_layout));

        mMainPresenter = new TestTakingPresenter(mStatsView, questionView, this, getSupportLoaderManager());

        Button button = (Button) findViewById(R.id.next_button);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (!mMainPresenter.showNextQuestion()) {
            ResultCalculator calculator = new ResultCalculator();
            calculator.calculate(mMainPresenter.getUserResponses());

            TestTakingTable table = new TestTakingTable();
            table.saveResult(calculator.getPercents());

            ResultActivity.start(this, mMainPresenter.getUserResponses());
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "On resume");
        mMainPresenter.showQuestions();
        mStatsView.startTimers();
    }

    @Override
    protected void onPause() {
        Log.d(LOG_TAG, "On pause");
        mStatsView.stopTimers();
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(LOG_TAG, "Saving instance");
        mMainPresenter.saveState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(LOG_TAG, "Restore instance");
        mMainPresenter.restoreState(savedInstanceState);
    }

    @Override
    protected void onStop() {
        Log.d(LOG_TAG, "On stop");
        super.onStop();
    }
}
