package com.magycbytes.ocajavatest.stories.resultTable.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.magycbytes.ocajavatest.database.TestTakingTable;
import com.magycbytes.ocajavatest.stories.resultTable.TestTaking;

import java.util.List;

/**
 * Created by Alex on 02/11/2016.
 */

public class TestTakingLoader extends AsyncTaskLoader<List<TestTaking>> {

    private List<TestTaking> mOldData;

    public TestTakingLoader(Context context) {
        super(context);
    }

    @Override
    public List<TestTaking> loadInBackground() {
        TestTakingTable takingTable = new TestTakingTable();
        return takingTable.loadAll();
    }

    @Override
    public void deliverResult(List<TestTaking> data) {
        mOldData = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mOldData != null) deliverResult(mOldData);
        if (takeContentChanged() || mOldData == null) forceLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        mOldData = null;
    }
}
