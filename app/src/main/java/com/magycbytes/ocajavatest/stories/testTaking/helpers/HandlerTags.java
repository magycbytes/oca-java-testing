package com.magycbytes.ocajavatest.stories.testTaking.helpers;

import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.style.TypefaceSpan;

import org.xml.sax.XMLReader;

/**
 * Created by Alex on 31/10/2016.
 */

public class HandlerTags implements Html.TagHandler {

    @Override
    public void handleTag(boolean opening, String tag, Editable output, XMLReader xmlReader) {
        if (tag.equalsIgnoreCase("span")) {
            if(opening) {
                output.setSpan(new TypefaceSpan("monospace"), output.length(), output.length(), Spannable.SPAN_MARK_MARK);
            } else {
                Object obj = getLast(output, TypefaceSpan.class);
                int where = output.getSpanStart(obj);

                output.setSpan(new TypefaceSpan("monospace"), where, output.length(), 0);
            }
        }
    }

    private Object getLast(Editable text, Class kind) {
        Object[] objs = text.getSpans(0, text.length(), kind);
        if(objs.length == 0) {
            return null;
        } else {
            for (int i=objs.length; i > 0; i--) {
                if(text.getSpanFlags(objs[i-1]) == Spannable.SPAN_MARK_MARK) {
                    return objs[i-1];
                }
            }
            return null;
        }
    }
}
