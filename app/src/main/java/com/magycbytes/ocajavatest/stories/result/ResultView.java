package com.magycbytes.ocajavatest.stories.result;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.magycbytes.ocajavatest.R;
import com.magycbytes.ocajavatest.stories.testTaking.UserResponse;
import com.magycbytes.ocajavatest.util.ResultCalculator;

import java.util.List;

/**
 * Created by Alex on 02/11/2016.
 */

class ResultView {

    private TextView mCorrectAnswers;
    private TextView mWrongAnswers;
    private TextView mPercent;
    private ProgressBar mPercentBar;

    ResultView(View view) {
        mCorrectAnswers = (TextView) view.findViewById(R.id.correct_answers);
        mWrongAnswers = (TextView) view.findViewById(R.id.wrong_answers);
        mPercent = (TextView) view.findViewById(R.id.progress_value);
        mPercentBar = (ProgressBar) view.findViewById(R.id.progress_bar);
    }

    void show(List<UserResponse> responses) {
        ResultCalculator calculator = new ResultCalculator();
        calculator.calculate(responses);

        mPercent.setText(mCorrectAnswers.getContext().getString(R.string.percent, calculator.getPercents()));
        mPercentBar.setProgress(calculator.getPercents());

        mCorrectAnswers.setText(String.valueOf(calculator.getCorrectAnswers()));
        mWrongAnswers.setText(String.valueOf(calculator.getWrongAnswers()));
    }
}
