package com.magycbytes.ocajavatest.stories.resultTable;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

import com.magycbytes.ocajavatest.R;
import com.magycbytes.ocajavatest.stories.resultTable.loader.TestTakingLoader;

import java.util.List;

public class ResultTableActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<TestTaking>> {

    private ResultTableView mTableView;

    public static void start(Context context) {
        context.startActivity(new Intent(context, ResultTableActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_table);

        mTableView = new ResultTableView(findViewById(R.id.activity_result_table));

        getSupportLoaderManager().initLoader(2, null, this);
    }

    @Override
    public Loader<List<TestTaking>> onCreateLoader(int i, Bundle bundle) {
        return new TestTakingLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<List<TestTaking>> loader, List<TestTaking> o) {
        mTableView.setAdapter(new TestTakingAdapter(o));
    }

    @Override
    public void onLoaderReset(Loader<List<TestTaking>> loader) {

    }
}
