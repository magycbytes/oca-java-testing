package com.magycbytes.ocajavatest.stories.testTaking.helpers;

/**
 * Created by Alex on 31/10/2016.
 */

public class HtmlEntities {

    private static final String DOUBLE_SPACE = "<br><br>";

    public static String decode(String original) {
        original = original.replaceAll("&amp;", "&");
        int index = original.indexOf(DOUBLE_SPACE);
        if (index != -1) {
            original = original.substring(index + DOUBLE_SPACE.length());
        }
        return original;
    }
}
