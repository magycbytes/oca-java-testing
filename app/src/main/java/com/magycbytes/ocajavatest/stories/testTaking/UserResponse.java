package com.magycbytes.ocajavatest.stories.testTaking;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alex on 01/11/2016.
 */

public class UserResponse implements Parcelable {

    private int mQuestionId;
    private String mResponse;
    private String mCorrectResponse;
    private String mQuestionNumber;

    public UserResponse() {

    }

    UserResponse(Parcel in) {
        mQuestionId = in.readInt();
        mResponse = in.readString();
        mCorrectResponse = in.readString();
        mQuestionNumber = in.readString();
    }

    public static final Creator<UserResponse> CREATOR = new Creator<UserResponse>() {
        @Override
        public UserResponse createFromParcel(Parcel in) {
            return new UserResponse(in);
        }

        @Override
        public UserResponse[] newArray(int size) {
            return new UserResponse[size];
        }
    };

    public int getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(int questionId) {
        mQuestionId = questionId;
    }

    public String getResponse() {
        return mResponse;
    }

    public void setResponse(String response) {
        mResponse = response;
    }

    public String getCorrectResponse() {
        return mCorrectResponse;
    }

    void setCorrectResponse(String correctResponse) {
        mCorrectResponse = correctResponse;
    }

    public String getQuestionNumber() {
        return mQuestionNumber;
    }

    public void setQuestionNumber(String questionNumber) {
        mQuestionNumber = questionNumber;
    }

    public boolean isResponseAvailable() {
        return mResponse != null && !mResponse.isEmpty();
    }

    public boolean isResponseCorrect() {
        return mCorrectResponse != null && mResponse != null && mResponse.equals(mCorrectResponse);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mQuestionId);
        parcel.writeString(mResponse);
        parcel.writeString(mCorrectResponse);
        parcel.writeString(mQuestionNumber);
    }
}
