package com.magycbytes.ocajavatest.stories.result;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.magycbytes.ocajavatest.R;
import com.magycbytes.ocajavatest.stories.testTaking.UserResponse;

/**
 * Created by Alex on 01/11/2016.
 */

class ResponseResultViewHolder extends RecyclerView.ViewHolder {

    private TextView mQuestionNumber;
    private ImageView mResultIcon;

    ResponseResultViewHolder(View itemView) {
        super(itemView);

        mQuestionNumber = (TextView) itemView.findViewById(R.id.question_number);
        mResultIcon = (ImageView) itemView.findViewById(R.id.result_icon);
    }

    void show(UserResponse userResponse) {
        mQuestionNumber.setText(userResponse.getQuestionNumber());

        int imageResourceId = ResponseIconFactory.getImageResourceId(userResponse.isResponseAvailable(), userResponse.isResponseCorrect());
        mResultIcon.setImageResource(imageResourceId);
    }
}
