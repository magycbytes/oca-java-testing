package com.magycbytes.ocajavatest.stories.testTaking.loader;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.google.gson.Gson;
import com.magycbytes.ocajavatest.R;
import com.magycbytes.ocajavatest.json.JsonFile;
import com.magycbytes.ocajavatest.json.Question;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Alex on 01/11/2016.
 */

public class TestLoader extends AsyncTaskLoader<TestLoaderResponse> {

    private static final String LOG_TAG = TestLoader.class.getSimpleName();
    private int QUESTIONS_COUNT = 20;

    private List<Integer> mOldIds;
    private TestLoaderResponse mOldResponse;

    public TestLoader(List<Integer> oldIds, Context context) {
        super(context);
        mOldIds = oldIds;

        String questionPrefKey = context.getString(R.string.exam_questions_number_key);
        QUESTIONS_COUNT = PreferenceManager.getDefaultSharedPreferences(context).getInt(questionPrefKey, 60);
    }

    @Override
    public TestLoaderResponse loadInBackground() {
        Log.d(LOG_TAG, "Load in background");

        List<Question> allQuestions = getQuestionsFromJson();

        TestLoaderResponse response = new TestLoaderResponse();
        response.setAllQuestions(allQuestions);

        if (mOldIds == null || mOldIds.size() != QUESTIONS_COUNT) {
            Collections.shuffle(allQuestions);

            extractNewQuestions(allQuestions, response);
        } else {
            findOldQuestions(allQuestions, response);
        }

        return response;
    }

    private void extractNewQuestions(List<Question> allQuestions, TestLoaderResponse response) {
        List<Question> testQuestions = allQuestions.subList(0, QUESTIONS_COUNT);
        List<Integer> testQuestionsIds = new ArrayList<>(QUESTIONS_COUNT);

        for (int i = 0; i < testQuestions.size(); ++i) {
            testQuestionsIds.add(testQuestions.get(i).getId());
        }
        response.setTestQuestions(testQuestions);
        response.setTestIds(testQuestionsIds);
    }

    private void findOldQuestions(List<Question> allQuestions, TestLoaderResponse response) {
        List<Question> oldQuestions = new ArrayList<>(QUESTIONS_COUNT);
        for (int i = 0; i < mOldIds.size(); ++i) {
            for (int j = 0; j < allQuestions.size(); ++i) {
                if (allQuestions.get(j).getId() == mOldIds.get(i)) {
                    oldQuestions.add(allQuestions.get(j));
                    break;
                }
            }
        }
        response.setTestQuestions(oldQuestions);
        response.setTestIds(mOldIds);
    }

    private List<Question> getQuestionsFromJson() {
        InputStream inputStream = getContext().getResources().openRawResource(R.raw.test_oca);
        JsonFile jsonFile = new Gson().fromJson(new BufferedReader(new InputStreamReader(inputStream)), JsonFile.class);
        return jsonFile.getSection().getQuestionList();
    }

    @Override
    public void deliverResult(TestLoaderResponse data) {
        Log.d(LOG_TAG, "Deliver result");
        mOldResponse = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        Log.d(LOG_TAG, "Start loading");
        if (mOldResponse != null) deliverResult(mOldResponse);
        if (takeContentChanged() || mOldResponse == null) forceLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        mOldResponse = null;
    }
}
