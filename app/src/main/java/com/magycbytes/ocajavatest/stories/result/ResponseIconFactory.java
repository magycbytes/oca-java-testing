package com.magycbytes.ocajavatest.stories.result;

import com.magycbytes.ocajavatest.R;

/**
 * Created by Alex on 02/11/2016.
 */

class ResponseIconFactory {

    static int getImageResourceId(boolean isResponseAvailable, boolean isResponseCorrect) {
        if (!isResponseAvailable) return R.drawable.ic_not_selected;
        if (isResponseCorrect) return R.drawable.ic_success;
        return R.drawable.ic_error;
    }
}
