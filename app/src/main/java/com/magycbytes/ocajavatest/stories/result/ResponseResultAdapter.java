package com.magycbytes.ocajavatest.stories.result;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.ocajavatest.R;
import com.magycbytes.ocajavatest.stories.testTaking.UserResponse;

import java.util.List;

/**
 * Created by Alex on 01/11/2016.
 */

class ResponseResultAdapter extends RecyclerView.Adapter<ResponseResultViewHolder> {

    private List<UserResponse> mResponseList;

    ResponseResultAdapter(List<UserResponse> responseList) {
        mResponseList = responseList;
    }

    @Override
    public ResponseResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_response_result, parent, false);
        return new ResponseResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ResponseResultViewHolder holder, int position) {
        holder.show(mResponseList.get(position));
    }

    @Override
    public int getItemCount() {
        return mResponseList.size();
    }
}
