package com.magycbytes.ocajavatest.stories.testTaking;

import android.os.Handler;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alex on 01/11/2016.
 */

class TimeTextView implements Runnable {

    private static final String LOG_TAG = TimeTextView.class.getSimpleName();

    private TextView mTimeShower;
    private long mElapsedTime = 0;
    private Handler mTimeHandler = new Handler();

    TimeTextView(TextView timeShower) {
        mTimeShower = timeShower;
    }

    @Override
    public void run() {
        updateTime();
        startTimer();
    }

    void reset() {
        stop();
        mElapsedTime = -1;
        run();
    }

    void setElapsedTime(long elapsedTime) {
        mElapsedTime = elapsedTime;
        showElapsedTime();
    }

    void stop() {
        mTimeHandler.removeCallbacks(this);
    }

    long getElapsedTime() {
        return mElapsedTime;
    }

    private void startTimer() {
        mTimeHandler.postDelayed(this, TimeUnit.SECONDS.toMillis(1));
    }

    private void updateTime() {
//        Log.d(LOG_TAG, "Time was updated");
        mElapsedTime++;
        showElapsedTime();
    }

    private void showElapsedTime() {
        long minutes = TimeUnit.SECONDS.toMinutes(mElapsedTime);
        int seconds = (int) (mElapsedTime - (minutes * TimeUnit.MINUTES.toSeconds(1)));

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, (int) minutes);
        calendar.set(Calendar.SECOND, seconds);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss", Locale.getDefault());
        String formattedTime = simpleDateFormat.format(calendar.getTime());

        mTimeShower.setText(formattedTime);
    }
}
