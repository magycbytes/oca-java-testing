package com.magycbytes.ocajavatest.stories.mainMenu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.magycbytes.ocajavatest.R;
import com.magycbytes.ocajavatest.stories.resultTable.ResultTableActivity;
import com.magycbytes.ocajavatest.stories.settings.SimpleSettingsActivity;
import com.magycbytes.ocajavatest.stories.testTaking.TestTakingActivity;

public class MainMenuActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        Button startTest = (Button) findViewById(R.id.start_test);
        startTest.setOnClickListener(this);

        Button viewResults = (Button) findViewById(R.id.view_results);
        viewResults.setOnClickListener(this);

        Button settings = (Button) findViewById(R.id.settings);
        settings.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.start_test) TestTakingActivity.start(this);
        else if (view.getId() == R.id.view_results) ResultTableActivity.start(this);
        else if (view.getId() == R.id.settings) SimpleSettingsActivity.start(this);
    }
}
