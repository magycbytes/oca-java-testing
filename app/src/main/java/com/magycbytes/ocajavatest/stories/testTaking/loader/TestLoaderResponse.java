package com.magycbytes.ocajavatest.stories.testTaking.loader;

import com.magycbytes.ocajavatest.json.Question;

import java.util.List;

/**
 * Created by Alex on 01/11/2016.
 */

public class TestLoaderResponse {

    private List<Integer> mTestIds;
    private List<Question> mAllQuestions;
    private List<Question> mTestQuestions;

    public List<Integer> getTestIds() {
        return mTestIds;
    }

    public void setTestIds(List<Integer> testIds) {
        mTestIds = testIds;
    }

    public List<Question> getAllQuestions() {
        return mAllQuestions;
    }

    public void setAllQuestions(List<Question> allQuestions) {
        mAllQuestions = allQuestions;
    }

    public List<Question> getTestQuestions() {
        return mTestQuestions;
    }

    public void setTestQuestions(List<Question> testQuestions) {
        mTestQuestions = testQuestions;
    }
}
