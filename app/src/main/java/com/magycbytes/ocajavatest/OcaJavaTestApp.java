package com.magycbytes.ocajavatest;

import android.app.Application;

import com.magycbytes.ocajavatest.database.JavaTestSqlHelper;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Alex on 02/11/2016.
 */

public class OcaJavaTestApp extends Application {

    public static JavaTestSqlHelper sTestSqlHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        sTestSqlHelper = new JavaTestSqlHelper(this);
    }

    @Override
    public void onTerminate() {
        sTestSqlHelper.close();
        sTestSqlHelper = null;
        super.onTerminate();
    }
}
