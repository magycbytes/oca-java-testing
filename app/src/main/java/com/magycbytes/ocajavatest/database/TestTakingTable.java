package com.magycbytes.ocajavatest.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.magycbytes.ocajavatest.OcaJavaTestApp;
import com.magycbytes.ocajavatest.stories.resultTable.TestTaking;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Alex on 02/11/2016.
 */

public class TestTakingTable extends DatabaseTable {

    private static final String TABLE_NAME = "TestTakingTable";
    private static final String TEST_TAKING_TIME = "TestTakingTime";
    private static final String TEST_SCORE = "TestScore";
    private static final String TEST_TAKING_ID = "TestTakingId";

    public TestTakingTable() {
        super(TABLE_NAME);
    }

    @Override
    public String getCreateSqlStatement() {
        addPrimaryKey(TEST_TAKING_ID);
        addIntegerColumn(TEST_TAKING_TIME);
        addIntegerColumn(TEST_SCORE);
        finishQuery();
        return mBuilder.toString();
    }

    public void saveResult(int percent) {
        long time = Calendar.getInstance().getTime().getTime();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TEST_TAKING_TIME, time);
        contentValues.put(TEST_SCORE, percent);

        SQLiteDatabase writableDatabase = OcaJavaTestApp.sTestSqlHelper.getWritableDatabase();
        writableDatabase.insert(TABLE_NAME, null, contentValues);
    }

    public List<TestTaking> loadAll() {
        SQLiteDatabase database = OcaJavaTestApp.sTestSqlHelper.getReadableDatabase();

        String[] columns = new String[] {
                TEST_SCORE,
                TEST_TAKING_TIME
        };

        Cursor cursor = database.query(TABLE_NAME, columns, null, null, null, null, TEST_TAKING_TIME);
        List<TestTaking> allTestTakings = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                int testScore = cursor.getInt(0);
                long takingTime = cursor.getLong(1);

                TestTaking testTaking = new TestTaking();
                testTaking.setDate(takingTime);
                testTaking.setPercents(testScore);

                allTestTakings.add(testTaking);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return allTestTakings;
    }
}
