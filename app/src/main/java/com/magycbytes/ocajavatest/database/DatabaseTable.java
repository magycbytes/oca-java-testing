package com.magycbytes.ocajavatest.database;

/**
 * Created by Alex on 02/11/2016.
 */

public abstract class DatabaseTable {

    protected StringBuilder mBuilder = new StringBuilder();

    protected DatabaseTable(String tableName) {
        mBuilder.append("CREATE TABLE IF NOT EXISTS ")
                .append(tableName)
                .append("(");
    }

    protected void addPrimaryKey(String columnName) {
        mBuilder.append(columnName)
                .append(" INTEGER PRIMARY KEY AUTOINCREMENT");
    }

    protected void addIntegerColumn(String columnName) {
        mBuilder.append(", ")
                .append(columnName)
                .append(" INTEGER");
    }

    void finishQuery() {
        mBuilder.append(");");
    }

    public abstract String getCreateSqlStatement();
}
