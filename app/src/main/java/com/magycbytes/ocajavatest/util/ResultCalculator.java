package com.magycbytes.ocajavatest.util;

import com.magycbytes.ocajavatest.stories.testTaking.UserResponse;

import java.util.List;

/**
 * Created by Alex on 02/11/2016.
 */

public class ResultCalculator {

    private int mCorrectAnswers;
    private int mWrongAnswers;
    private int mPercents;

    public void calculate(List<UserResponse> responses) {
        mCorrectAnswers = 0;
        mWrongAnswers = 0;
        mPercents = 0;

        for (UserResponse response : responses) {
            if (response.isResponseCorrect()) mCorrectAnswers++;
            else mWrongAnswers++;
        }

        int totalAnswers = responses.size();
        mPercents = 100 / totalAnswers * mCorrectAnswers;
    }

    public int getCorrectAnswers() {
        return mCorrectAnswers;
    }

    public int getWrongAnswers() {
        return mWrongAnswers;
    }

    public int getPercents() {
        return mPercents;
    }
}
