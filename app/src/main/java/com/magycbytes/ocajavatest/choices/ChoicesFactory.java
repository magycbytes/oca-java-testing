package com.magycbytes.ocajavatest.choices;

import android.widget.LinearLayout;

/**
 * Created by Alex on 31/10/2016.
 */

public class ChoicesFactory {

    public static BaseChoice getChoice(String answers, LinearLayout linearLayout) {
        String[] responses = answers.split(";");
        if (responses.length > 1) return new MultipleChoice(linearLayout);
        else return new SingleChoice(linearLayout);
    }
}
