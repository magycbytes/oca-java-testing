package com.magycbytes.ocajavatest.choices;

import android.text.Spanned;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * Created by Alex on 31/10/2016.
 */

class SingleChoice extends BaseChoice {

    private RadioGroup mRadioGroup;

    SingleChoice(LinearLayout rootLayout) {
        super(rootLayout);

        mRadioGroup = new RadioGroup(mContext);
        addView(mRadioGroup);
    }

    @Override
    public void addResponseButton(Spanned spanned) {
        RadioButton radioButton = new RadioButton(mContext);
        radioButton.setText(spanned);

        mRadioGroup.addView(radioButton);
    }

    @Override
    public String getChoices() {
        if (mRadioGroup == null) return "";

        for (int i = 0; i < mRadioGroup.getChildCount(); ++i) {
            if (((RadioButton)mRadioGroup.getChildAt(i)).isChecked()) {
                return String.valueOf(i + 1);
            }
        }

        return "";
    }
}
