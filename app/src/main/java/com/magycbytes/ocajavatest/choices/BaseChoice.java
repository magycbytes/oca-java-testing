package com.magycbytes.ocajavatest.choices;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.LinearLayout;

import com.magycbytes.ocajavatest.stories.testTaking.helpers.HandlerTags;
import com.magycbytes.ocajavatest.stories.testTaking.helpers.HtmlEntities;
import com.magycbytes.ocajavatest.json.ChoiceData;
import com.magycbytes.ocajavatest.json.ChoiceDetails;

import java.util.List;

/**
 * Created by Alex on 31/10/2016.
 */

public abstract class BaseChoice {

    protected LinearLayout mRootLayout;
    Context mContext;

    BaseChoice(LinearLayout rootLayout) {
        mRootLayout = rootLayout;
        mContext = mRootLayout.getContext();
    }

    public void addResponses(List<ChoiceData> choicesList) {
        for (int j = 0; j < choicesList.size(); ++j) {
            List<ChoiceDetails> details = choicesList.get(j).getDetails();
            String data = details.get(0).getData();
            Spanned spanned = Html.fromHtml(HtmlEntities.decode(data), null, new HandlerTags());

            addResponseButton(spanned);
        }
    }

    void addView(View view) {
        mRootLayout.addView(view);
    }

    public abstract void addResponseButton(Spanned spanned);

    public abstract String getChoices();
}
