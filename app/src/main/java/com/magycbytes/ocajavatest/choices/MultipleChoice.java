package com.magycbytes.ocajavatest.choices;

import android.text.Spanned;
import android.widget.CheckBox;
import android.widget.LinearLayout;

/**
 * Created by Alex on 31/10/2016.
 */

class MultipleChoice extends BaseChoice {

    MultipleChoice(LinearLayout rootLayout) {
        super(rootLayout);
    }

    @Override
    public void addResponseButton(Spanned spanned) {
        CheckBox checkBox = new CheckBox(mContext);
        checkBox.setText(spanned);
        addView(checkBox);
    }

    @Override
    public String getChoices() {
        String choices = "";

        for (int i = 0; i < mRootLayout.getChildCount(); ++i) {
            if (((CheckBox) mRootLayout.getChildAt(i)).isChecked()) {
                if (!choices.isEmpty()) choices += ";";
                choices += String.valueOf(i + 1);
            }
        }

        return choices;
    }
}
