package com.magycbytes.ocajavatest.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Alex on 31/10/2016.
 */

public class ChoiceData {

    @SerializedName("id")
    private int mChoiceId;

    @SerializedName("data")
    private List<ChoiceDetails> mDetails;

    public int getChoiceId() {
        return mChoiceId;
    }

    public void setChoiceId(int choiceId) {
        mChoiceId = choiceId;
    }

    public List<ChoiceDetails> getDetails() {
        return mDetails;
    }

    public void setDetails(List<ChoiceDetails> details) {
        mDetails = details;
    }
}
