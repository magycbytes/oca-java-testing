package com.magycbytes.ocajavatest.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 31/10/2016.
 */

public class DisplayOrder {

    @SerializedName("display_order")
    private int mDisplayOrder;
}
