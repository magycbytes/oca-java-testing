package com.magycbytes.ocajavatest.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Alex on 31/10/2016.
 */

public class Section {

    @SerializedName("question")
    private List<Question> mQuestionList;

    public List<Question> getQuestionList() {
        return mQuestionList;
    }

    public void setQuestionList(List<Question> questionList) {
        mQuestionList = questionList;
    }
}
