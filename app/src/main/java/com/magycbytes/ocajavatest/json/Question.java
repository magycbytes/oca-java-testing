package com.magycbytes.ocajavatest.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Alex on 31/10/2016.
 */

public class Question {

    @SerializedName("answer")
    private String mAnswerNumber;

    @SerializedName("difficulty_level")
    private String mDifficultyLevel;

    @SerializedName("assessment_title")
    private String mAssesementTitle;

    @SerializedName("learning_objective")
    private String mLearningObjective;

    @SerializedName("hints")
    private List<Hint> mHintList;

    @SerializedName("choiceData")
    private List<ChoiceData> mChoiceDataList;

    @SerializedName("questionData")
    private List<QuestionData> mQuestionDataList;

    @SerializedName("id")
    private int mId;

    public String getAnswerNumber() {
        return mAnswerNumber;
    }

    public List<ChoiceData> getChoiceDataList() {
        return mChoiceDataList;
    }

    public List<QuestionData> getQuestionDataList() {
        return mQuestionDataList;
    }

    public int getId() {
        return mId;
    }
}
