package com.magycbytes.ocajavatest.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 31/10/2016.
 */

public class QuestionData extends DataType {

    @SerializedName("data")
    private String mData;

    public String getData() {
        return mData;
    }
}
