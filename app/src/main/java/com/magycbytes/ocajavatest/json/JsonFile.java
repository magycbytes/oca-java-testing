package com.magycbytes.ocajavatest.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 31/10/2016.
 */

public class JsonFile {

    @SerializedName("sections")
    private Section mSection;

    public Section getSection() {
        return mSection;
    }

    public void setSection(Section section) {
        mSection = section;
    }
}
