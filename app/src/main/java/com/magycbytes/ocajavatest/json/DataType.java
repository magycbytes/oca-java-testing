package com.magycbytes.ocajavatest.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 31/10/2016.
 */

public class DataType extends DisplayOrder {

    @SerializedName("data_type_id")
    private int mDataTypeId;
}
